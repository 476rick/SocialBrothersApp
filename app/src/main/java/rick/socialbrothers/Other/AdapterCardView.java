package rick.socialbrothers.Other;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;

import java.util.List;

import rick.socialbrothers.R;

public class AdapterCardView extends RecyclerView.Adapter<AdapterCardView.StockViewHolder>{
    private List<Card> cards;

    public AdapterCardView(List<Card> cards){
        this.cards = cards;
    }

    @Override
    public StockViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout, parent, false);
        return new StockViewHolder(v);
    }

    @Override //Inflate all data into the Card
    public void onBindViewHolder(StockViewHolder holder, int position) {
        holder.cardTitle.setText(cards.get(position).getTitle());
        holder.cardSubtitle.setText(cards.get(position).getSubtitle());
        Ion.with(holder.cardImage)
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .load(cards.get(position).getImageUrl());
    }

    @Override
    public int getItemCount() {
        return cards.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class StockViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView cardTitle;
        TextView cardSubtitle;
        ImageView cardImage;

        StockViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv);
            cardTitle = (TextView)itemView.findViewById(R.id.cardTitle);
            cardSubtitle = (TextView)itemView.findViewById(R.id.cardSubtitle);
            cardImage = (ImageView)itemView.findViewById(R.id.cardImage);
        }
    }

}
