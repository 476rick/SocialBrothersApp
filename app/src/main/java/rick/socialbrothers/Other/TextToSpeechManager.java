package rick.socialbrothers.Other;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.speech.tts.TextToSpeech;

import java.util.HashMap;
import java.util.Locale;

public class TextToSpeechManager {
    private static TextToSpeech t1;
    private static String utteranceID = "";
    private float speechRate;

    public TextToSpeechManager(Context c){
        speechRate = 0.7f;
        if(c != null) {
            try {
                t1 = new TextToSpeech(c, new TextToSpeech.OnInitListener() {
                    @Override
                    public void onInit(int status) {
                        if (status != TextToSpeech.ERROR) {
                            t1.setLanguage(Locale.getDefault()); //Get the local language
                            utteranceID = this.hashCode() + ""; //this needs to be declared here because you cannot call this in static method ttsGreaterThan21();
                            t1.setSpeechRate(speechRate);
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //Let the app speak.
    public static void speakString(String toSpeak, Context context){
        if(t1 == null){
            new TextToSpeechManager(context);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ttsGreater21(toSpeak);
        } else {
            ttsUnder20(toSpeak);
        }
    }

    @SuppressWarnings("deprecation")
    private static void ttsUnder20(String text) {
        HashMap<String, String> map = new HashMap<>();
        map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "MessageId");
        t1.speak(text, TextToSpeech.QUEUE_FLUSH, map);

    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private static void ttsGreater21(String text) {
        t1.speak(text, TextToSpeech.QUEUE_FLUSH, null, utteranceID);
    }
}
