package rick.socialbrothers.Activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

import rick.socialbrothers.Fragments.FragItem1;
import rick.socialbrothers.Fragments.FragItem2;
import rick.socialbrothers.Fragments.FragItem3;
import rick.socialbrothers.Other.TextToSpeechManager;
import rick.socialbrothers.R;

public class MainActivity extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextToSpeechManager.speakString("", getApplicationContext()); //initialize TTS

        //Navigation drawer
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        // Set the adapter for the list view
        mDrawerList.setAdapter(new ArrayAdapter<>(this, R.layout.drawer_list_item, getResources().getStringArray(R.array.fragments_array)));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {
            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true); //show the up navigation (hamburger menu)
        getSupportActionBar().setHomeButtonEnabled(true);

        //load fragment item 1
        setFragment(new FragItem1());
        mDrawerList.setItemChecked(0, true); //because the first item is always loaded

        BottomBar bottomBar = (BottomBar) findViewById(R.id.bottomBar);
        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                switch(tabId){
                    case R.id.tab_item_1:
                        setTitle("Item 1");
                        setFragment(new FragItem1());
                        mDrawerList.setItemChecked(0, true);
                        break;
                    case R.id.tab_item_2:
                        setTitle("Item 2");
                        setFragment(new FragItem2());
                        mDrawerList.setItemChecked(1, true);
                        break;
                    case R.id.tab_item_3:
                        setTitle("Item 3");
                        setFragment(new FragItem3());
                        mDrawerList.setItemChecked(2, true);
                        break;
                    default:
                        break;
                }
            }
        });
    }

    //dynamically replace the fragment in the activity
    private void setFragment(Fragment fragment){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.contentContainer, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.custom_menu, menu);
        return true; //inflate the menu
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case R.id.action_settings: //settings pressed
                Intent myIntent = new Intent(MainActivity.this, SettingActivity.class);
                this.startActivity(myIntent); //load new activity
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    private void selectItem(int position) {
        BottomBar bottomBar = (BottomBar) findViewById(R.id.bottomBar);
        if(bottomBar == null){  return; } //bottombar can be null

        switch(position){
            case 0:
                bottomBar.selectTabAtPosition(0);
                break;
            case 1:
                bottomBar.selectTabAtPosition(1);
                break;
            case 2:
                bottomBar.selectTabAtPosition(2);
                break;
        }

        mDrawerList.setItemChecked(position, true); //navigation drawer set selected
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
}
