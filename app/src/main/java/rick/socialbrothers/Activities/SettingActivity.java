package rick.socialbrothers.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import rick.socialbrothers.R;

public class SettingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
    }

    public void clickMe(View view) { //show message when click on button
        Toast.makeText(this, R.string.click_me_text, Toast.LENGTH_SHORT).show();
    }
}
