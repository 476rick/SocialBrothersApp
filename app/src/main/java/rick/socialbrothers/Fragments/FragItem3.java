package rick.socialbrothers.Fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONObject;

import rick.socialbrothers.Other.JSONHelper;
import rick.socialbrothers.Other.TextToSpeechManager;
import rick.socialbrothers.R;

public class FragItem3 extends Fragment {
    private static String degrees = "";
    private static String weatherCondition = "";
    private static String windMph = "";
    private static String locationStatus;

    public FragItem3() { } // Required empty public constructor

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_frag_item3, container, false);

        //add checklocation listener to button
        Button checkLocationButton = (Button) view.findViewById(R.id.check_location);
        checkLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View parent = getView();
                TextView locationStatusTextView = null;
                EditText locationSearch = null;
                if(parent != null) {
                    locationStatusTextView = (TextView) parent.findViewById(R.id.location_status);
                    locationSearch = (EditText)parent.findViewById(R.id.location_search);
                }
                if(!locationSearch.getText().toString().equals("")) {
                    locationStatusTextView.setText(R.string.loading);
                    new GetWeatherByLocation().execute(locationSearch.getText().toString());
                } else {
                    locationStatusTextView.setText(R.string.no_location_given);
                }
            }
        });

        //Generic Listener for all buttons that need to speak
        View.OnClickListener speakOnClickListener = new View.OnClickListener() {
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.fragment3_degrees:
                        if(!isWeatherVariableEmpty(degrees)){
                            TextToSpeechManager.speakString("The temperature is " + degrees + " degrees.", getContext());
                        }
                        break;
                    case R.id.fragment3_wind:
                        if(!isWeatherVariableEmpty(windMph)){
                            TextToSpeechManager.speakString("The wind speed is " + windMph + " miles per hour.", getContext());
                        }
                        break;
                    case R.id.fragment3_condition:
                        if(!isWeatherVariableEmpty(weatherCondition)){
                            TextToSpeechManager.speakString("The weather condition is " + weatherCondition, getContext());
                        }
                        break;
                }
            }
        };

        //add listener to the buttons
        Button degreesButton = (Button) view.findViewById(R.id.fragment3_degrees);
        degreesButton.setOnClickListener(speakOnClickListener);
        Button windButton = (Button) view.findViewById(R.id.fragment3_wind);
        windButton.setOnClickListener(speakOnClickListener);
        Button conditionButton = (Button) view.findViewById(R.id.fragment3_condition);
        conditionButton.setOnClickListener(speakOnClickListener);
        return view;
    }

    private boolean isWeatherVariableEmpty(String weatherVariable){
        if(weatherVariable.equals("")){ //location hasn't been set so first do that
            TextToSpeechManager.speakString(getString(R.string.first_fill_in_location), getContext());
            return true;
        }
        return false;
    }

    private class GetWeatherByLocation extends AsyncTask<String, Void, JSONObject> {
        protected JSONObject doInBackground(String... url) {
            JSONObject jsonObject = null;
            try { //connect with API
                jsonObject = JSONHelper.readJsonFromUrl(" http://api.apixu.com/v1/current.json?key=9725be56299945c59e7161926170601&q=" + url[0]);
                String errorString  = jsonObject.optString("error");

                if(!errorString.equals("")){ //when there is an error this is not equal to ""
                    JSONObject errorObject = jsonObject.getJSONObject("error");
                    locationStatus = errorObject.getString("message");
                } else { //correct API call
                    JSONObject locationInfo = jsonObject.getJSONObject("location");
                    locationStatus = "Location: " + locationInfo.getString("name");
                    JSONObject currentWeatherInfo = jsonObject.getJSONObject("current");
                    degrees = currentWeatherInfo.getString("temp_c");
                    windMph = currentWeatherInfo.getString("wind_mph");
                    JSONObject currentCondition = currentWeatherInfo.getJSONObject("condition");
                    weatherCondition = currentCondition.getString("text");
                }
            } catch (Exception e) { //IOException or JSONException
                e.printStackTrace();
            }
            return jsonObject;
        }

        protected void onPostExecute(JSONObject jsonObject) {
            View parent = getView();
            if(parent != null){ //fill the textViews in the fragment with the info
                TextView locationStatusTextView = (TextView)parent.findViewById(R.id.location_status);
                TextView degreesTextView = (TextView)parent.findViewById(R.id.degrees);
                TextView windTextView = (TextView)parent.findViewById(R.id.wind);
                TextView conditionTextView = (TextView)parent.findViewById(R.id.condition);

                //set text of textviews
                locationStatusTextView.setText(locationStatus);
                degreesTextView.setText(degrees + getString(R.string.degrees));
                windTextView.setText(windMph + getString(R.string.mph));
                conditionTextView.setText(weatherCondition);
            }
        }
    }
}
