package rick.socialbrothers.Fragments;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import rick.socialbrothers.Other.JSONHelper;
import rick.socialbrothers.R;

public class FragItem2 extends Fragment {
    private static final Map<String, String> newsTitlesAndLinks = new HashMap<>();

    public FragItem2() { } // Required empty public constructor

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_frag_item2, container, false);

        //ASYNC: get the titles of newsArticles and fill Listview
        new ReadNewsArticles().execute("https://newsapi.org/v1/articles?source=bbc-news&sortBy=top&apiKey=c97df96dd4dc4d9399a2bd3c0ffa1ffc");

        return view;
    }

    private class ReadNewsArticles extends AsyncTask<String, Void, JSONObject> {
        protected JSONObject doInBackground(String... url) {
            JSONObject json = null;
            try {
                json = JSONHelper.readJsonFromUrl(url[0]);
            } catch (Exception e) { //IOException or JSONException
                e.printStackTrace();
            }
            return json;
        }

        protected void onPostExecute(JSONObject JSONObject) {
            final ArrayList<String> newsTitles;
            System.out.println(JSONObject.toString());

            JSONArray jsonArray = null;
            try {
                jsonArray = JSONObject.getJSONArray("articles"); //get articles array
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i); //get specific JSONObject
                   // newsTitles.add(object.getString("title")); //add title attribute from JSONObject

                    newsTitlesAndLinks.put(object.getString("title"), object.getString("url"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            View v = getView();
            if(newsTitlesAndLinks.size() > 0 && v != null){
                ListView newsListView = (ListView)v.findViewById(R.id.fragment2_listview);

                //parse Set<String> to ArrayList<String>
                newsTitles = new ArrayList<>(newsTitlesAndLinks.keySet());

                //fill listView in fragment 2
                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, newsTitles);
                newsListView.setAdapter(arrayAdapter);

                newsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                            long arg3) {
                        //Open the BBC Link of the article
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(newsTitlesAndLinks.get(newsTitles.get(arg2))));
                        startActivity(browserIntent);
                    }
                });
            }
        }
    }
}
