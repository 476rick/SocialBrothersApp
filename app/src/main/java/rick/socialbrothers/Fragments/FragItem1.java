package rick.socialbrothers.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import rick.socialbrothers.Other.AdapterCardView;
import rick.socialbrothers.Other.Card;
import rick.socialbrothers.R;

public class FragItem1 extends Fragment {

    public FragItem1() {}  // Required empty public constructor

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_frag_item1, container, false);
        RecyclerView listOfCards = (RecyclerView) view.findViewById(R.id.list_of_cards);

        List<Card> cards = new ArrayList<>();
        cards.add(new Card("Social Brothers", "Android Application", "https://d13yacurqjgara.cloudfront.net/users/555465/screenshots/2088288/flower_copy_1x.jpg"));
        cards.add(new Card("Beelscherm", "Tvtje kopen", "http://mashable.com/wp-content/uploads/2011/07/flatscreen-lcd-tv-640.jpg"));
        cards.add(new Card("Vierkantje", "Mooi bloemen vierkantje", "http://www.myfreephotoshop.com/wp-content/uploads/2013/09/509.jpg"));
        cards.add(new Card("Bewegelijk", "Het knipoogt", "http://www.animaties.com/data/media/527/hart-met-gezicht-bewegende-animatie-0112.gif"));
        cards.add(new Card("Poppetje", "Verrekijker", "https://holidaymedia.nl/cmslib/www.holidaymedia.nl/holidaymedia/nieuws/huge/Fotografie-200x200.jpg"));
        cards.add(new Card("Galerij", "Bekijk afbeeldingen", "http://www.thatsocialrascal.nl/wp-content/uploads/2014/04/Afbeeldingen.jpg"));
        cards.add(new Card("Google", "Credits aan Google", "https://articles-images.sftcdn.net/wp-content/uploads/sites/5/2013/01/Google-afbeeldingen.png"));
        cards.add(new Card("Plan", "Voor  de mooie plaatjes", "http://f.jwwb.nl/public/h/a/z/temp-djqecvzwdkpsaknktuck/logo-planbelgie-200x2001.png"));
        cards.add(new Card("Apart", "Leuke foto", "http://www.huntart.nl/wp-content/gallery/tekstbijafb/thumbs/thumbs_03.jpg"));
        cards.add(new Card("Hondenspeelgoed", "Kwam ik tegen", "http://www.ellensdierenwinkel.nl/contents/media/l_pipo020.jpg"));

        //Add layout manager to the recyclerview
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        listOfCards.setLayoutManager(llm);
        listOfCards.setAdapter(new AdapterCardView(cards));

        return view;
    }

}
